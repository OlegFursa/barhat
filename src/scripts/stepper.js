$('document').ready( function () {

    $('.btn-minus').click(function () {
        var $input = $('.counter-input');
        var count = parseFloat($input.val()) - 0.5;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.btn-plus').click(function () {
        var $input = $('.counter-input');
        $input.val(parseFloat($input.val()) + 0.5);
        $input.change();
        return false;
    });


});
