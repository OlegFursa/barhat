$(function () {
    var mySwiper = new Swiper('.main-slider', {
        slidesPerView: 1,
        autoplay: 5000,
        pagination: ('.swiper-pagination'),
        loop: true
    });
    var productSwiper = new Swiper('.product-slider', {
        slidesPerView: 1,
        autoplay: 5000,
        pagination: ('.swiper-pagination'),
        loop: true
    });

    var navSwiper = new Swiper('.nav-slider', {
        slidesPerView: 'auto',
        freeMode: true,
        grabCursor: true
    });


    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        direction: 'vertical'
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        direction: 'vertical',
        slidesPerView: 3,
        touchRatio: 0.2,
        slideToClickedSlide: true,
        onClick: function (swiper, event) {

            galleryTop.slideTo(swiper.clickedIndex, 300, function () {});

        }
    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;

});


