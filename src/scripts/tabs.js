$('document').ready( function () {
    $('.switch').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('.switch').removeClass('active');
        $('.tabs-content').removeClass('active');

        $(this).addClass('active');
        $("#"+tab_id).addClass('active');

    });
});