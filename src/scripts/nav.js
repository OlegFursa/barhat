$('document').ready( function () {
    $('.dropdown-btn').click( function() {
        if($('.dropdown-inner').hasClass('active')) {
            $('.dropdown-inner').removeClass('active');
        } else {
            $('.dropdown-inner').addClass('active');
        }
    });



    $('.mob-btn').click(function() {
        $(this).toggleClass('active');
        $('.main-nav').toggleClass('visible');
        if($(this).hasClass('active')) {
            $(this).parents('.header').find('.logo').css('display', 'none');
            $('body').css("overflow", "hidden");
        } else {
            $(this).parents('.header').find('.logo').css('display', 'inline-block');
            $('body').css("overflow", "visible");
        }
    });

    $('.del-btn').click(function () {
        $(this).parents('.cart-item').remove();
    });

    var searchBtn = $('#search-btn');

    searchBtn.click(function () {
        if(searchBtn.hasClass('search-submit')) {
            $( "#search-form" ).submit();
        } else {
            searchBtn.addClass('search-submit');
            $('.search-input').addClass('visible');
            $('.close-search').addClass('visible');
        }
    });

    $('.close-search').click(function () {
        $(this).removeClass('visible');
        $('.search-input').removeClass('visible');
        searchBtn.removeClass('search-submit');
    });

    $('.out-of-stock').html('Под заказ');

});

$(document).mouseup(function (e) {
    var container = $(".dropdown-inner");
    var searchInput = $(".search-input");

    if (container.has(e.target).length === 0){
        container.removeClass('active');
    }

    // if (searchInput.has(e.target).length === 0){
    //     searchInput.removeClass('visible');
    // }
});
