$(document).ready(function(){
    $('.dropdown-inner .size').click(function(){
        var val = $(this).html();
        var val_slug = $(this).attr('data-slug');
        $('.size.current-size').html(val);
        $('.dropdown-inner .size').removeClass('active');
        $(this).addClass('active');
        $('#attribute_pa_size').val(val_slug);
        $('.dropdown-inner').removeClass('active');
    });
});

$(window).on('load', function () {
    if ( window.location.href.indexOf('product-category') !== -1 ){
        $('body,html').animate({
            scrollTop: $('.tabs-container').offset().top - 160}, 500);
        return false;
    }
});