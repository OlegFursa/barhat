<?php
get_header(); ?>
    <section class="p-b0">
        <div class="max-w-1000 m-center pad-l3 pad-r3 d-flex">
            <?php do_action( 'woocommerce_before_main_content' ); ?>
        </div>
    </section>

    <section>
        <div class="max-w-1000 m-center pad-l3 pad-r3">
<!--            <h1>--><?php //single_cat_title(); ?><!--</h1>-->
        </div>
        <div class="d-flex p-b5 news-wrapper max-w-1000 m-center pad-l3 pad-r3">
            <?php
            if ( have_posts() ) : ?>
                <?php
                /* Start the Loop */
                while ( have_posts() ) : the_post();
                    news_template(get_the_title(), get_the_ID());
                endwhile;

            endif; ?>
        </div>
        <div class="d-flex p-b5 news-wrapper max-w-1000 m-center pad-l3 pad-r3">
            <?php pagination(); ?>
        </div>
    </section>
<?php get_footer();
