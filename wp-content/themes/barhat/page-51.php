<?php
// Contact page
get_header(); ?>

<section class="max-w-1000 m-center pad-l3 pad-r3 p-b0">
    <?php do_action( 'woocommerce_before_main_content' ); ?>
<!--    <h1>--><?php //the_title(); ?><!--</h1>-->
</section>
<?php while ( have_posts() ) : the_post(); ?>
    <section>
        <?php if ($post->post_content != '') { ?>
            <div class="max-w-1000 m-center pad-l3 pad-r3 d-flex contact-wrapper">
                <?php the_content(); ?>
            </div>
        <?php }
        $custom_fields = get_post_custom();
        ?>
        <div class="max-w-1000 m-center pad-l3 pad-r3 d-flex contact-wrapper">
            <div class="decor"></div>
            <div class="contact-info">
                <span><?php echo get_field_object( 'stock_office' )['label']; ?></span>
                <p><?php echo $custom_fields['stock_office'][0]; ?></p>
                <span><?php echo get_field_object( 'shop' )['label']; ?></span>
                <p><?php echo $custom_fields['shop'][0]; ?></p>
                <span><?php echo get_field_object( 'phones' )['label']; ?></span>
                <p><?php echo str_replace('[br]', '<br />', $custom_fields['phones'][0]); ?></p>
            </div>
            <div class="map" id="map">
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_zq12LXaD_aaBH-qW2qRpZ8O0aT_PZOs&callback=initMap"
                        async defer></script>
            </div>
        </div>
    </section>
<?php endwhile; // End of the loop.

get_footer(); ?>
