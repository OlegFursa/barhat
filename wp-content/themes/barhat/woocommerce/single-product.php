<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header( 'shop' );

shop_before_prod();

while ( have_posts() ) : the_post();
    wc_get_template_part( 'content', 'single-product' );
endwhile; // end of the loop.

shop_after_prod();

do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
