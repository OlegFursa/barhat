<?php
/**
 * Single variation cart button
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $product;

$sizes = wc_get_product_terms( $product->get_id(), 'pa_size', array( 'fields' => 'all' ) );
?>
<div class="woocommerce-variation-add-to-cart variations_button">
    <button type="submit" class="button button-pink full-width"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
    <input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
    <input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
    <input type="hidden" name="variation_id" class="variation_id" value="0" />
    <input type="hidden" name="attribute_pa_size" id="attribute_pa_size" value="<?php echo ($sizes[0]->slug) ? $sizes[0]->slug : 0; ?>" />
</div>
