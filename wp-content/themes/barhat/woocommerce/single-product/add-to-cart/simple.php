<?php
if (!defined('ABSPATH')) {
    exit;
}

global $product;

$attribute_values = wc_get_product_terms($product->get_id(), 'pa_manufacturer', array('fields' => 'all'))[0];
$manufacturer = $attribute_values->name;

$attribute_values = wc_get_product_terms($product->get_id(), 'pa_running_meter', array('fields' => 'all'));
?>

<?php do_action('woocommerce_before_add_to_cart_form'); ?>

<form class="cart p-b5" method="post" enctype='multipart/form-data'>
	<div class="product-card p-b4">
        <?php wc_get_template_part('single-product/product', 'image'); ?>
		<div class="product-info pad-l6">
            <?php
            // if (get_parent_category_id() == 20) {
            echo '<h2 class="f-20">' . get_the_title() . '</h2>';
            // }
            ?>
			<div>
                <?php isa_woocommerce_all_pa(); ?>
                <?php if ($manufacturer || $product->width) { ?>
                    <?php if ($manufacturer) { ?>
						<div class="d-flex p-b2">
							<span><?php echo wc_attribute_label('pa_manufacturer'); ?>:</span>
							<span class="f-secodary"><?php echo $manufacturer; ?></span>
						</div>
                    <?php } ?>
                    <?php if ($product->width) { ?>
						<div class="d-flex">
							<span><?php echo __('Width', 'barhat'); ?>:</span>
							<span class="f-secodary"><?php echo $product->width; ?><?php echo __('m', 'barhat'); ?></span>
						</div>
                    <?php } ?>
                <?php } ?>
                <?php
                $sizes = wc_get_product_terms($product->get_id(), 'pa_size', array('fields' => 'all'));
                if (count($sizes) > 1) {
                    ?>
					<div class="dropdown p-b3">
						<div class="dropdown-hover">
							<span>Размер:</span>
							<span class="size current-size"><?php echo $sizes[0]->name; ?></span>
							<span class="dropdown-btn"><img src="/dist/images/interface/arrow-down.png"
															alt=""></span>
						</div>
						<div class="dropdown-inner">
                            <?php $i = 0;
                            foreach ($sizes as $s) {
                                $class = '';
                                if ($i == 0) $class = ' active';
                                echo '<span class="size' . $class . '" data-slug="' . $s->slug . '">' . $s->name . '</span>';
                                $i++;
                            } ?>
						</div>
					</div>
                <?php } ?>
			</div>
			<div class="counter-wrapper p-b3">
                <?php if ($product->is_purchasable()) { ?>
					<div>
						<div><?php echo __('Price', 'barhat'); ?>:</div>
						<div>
							<span class="f-24"><?php wc_get_template_part('single-product/price', ''); ?> </span>
                            <?php if ($attribute_values[0]->term_id == 16) echo '<span class="c-dgray">/' . __('meter', 'barhat') . '</span>'; ?>
						</div>
					</div>
                    <?php
                    do_action('woocommerce_before_add_to_cart_quantity');
                }
                ?>
                <?php if ($product->is_purchasable()) { ?>
					<div class="counter-stepper">
						<button class="counter-btn btn-minus">-</button>
						<input type="text" name="quantity" value="1" class="counter-input"/>
						<button class="counter-btn btn-plus">+</button>
					</div>
                    <?php if ($attribute_values[0]->term_id == 16) echo '<span class="c-dgray">' . __('meters', 'barhat') . '</span>'; ?>
                <?php } ?>
			</div>
            <?php
            if ($product->is_purchasable()) {
                wc_get_template_part('single-product/add-to-cart/variation-add-to-cart', 'button');
            } ?>
		</div>

	</div>
    <?php if ('' !== get_post()->post_content) { ?>
		<h4><?php _e('About product', 'barhat'); ?></h4>
        <?php the_content(); ?>
    <?php } ?>
    <?php
    /**
     * @since 2.1.0.
     */
    do_action('woocommerce_before_add_to_cart_button');

    /**
     * @since 3.0.0.
     */

    /**
     * @since 3.0.0.
     */
    do_action('woocommerce_after_add_to_cart_quantity');
    ?>
    <?php
    /**
     * @since 2.1.0.
     */
    do_action('woocommerce_after_add_to_cart_button');
    ?>
</form>

<p class="customers-info">
	Для того, чтобы заказать этот товар, необходимо добавить его в «Корзину» и заполнить форму заказа. В ближайшее
	время мы с вами свяжемся и уточним все детали. Подробнее о том, как получить заказ, читайте на странице
	"Доставка".
</p>


<?php do_action('woocommerce_after_add_to_cart_form'); ?>
