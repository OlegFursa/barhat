<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.2
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $post, $product;
$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
$image_title       = get_post_field( 'post_excerpt', $post_thumbnail_id );
$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
    'woocommerce-product-gallery',
    'woocommerce-product-gallery--' . $placeholder,
    'woocommerce-product-gallery--columns-' . absint( $columns ),
    'images',
) );
?>
<?php
if ( has_post_thumbnail() ) {
    $attachment_ids = $product->get_gallery_attachment_ids();
    $html = '';
    if ($attachment_ids) {
        $html .= '<div class="product-preview-wrapper">
        <div class="swiper-container gallery-thumbs">
            <div class="swiper-wrapper">';
        $html .= '<div class="swiper-slide"  style="background-image: url(\'' . get_the_post_thumbnail_url($post->ID, 'medium') . '\')"></div>';
        foreach ($attachment_ids as $attachment_id) {
            $html .= '<div class="swiper-slide"  style="background-image: url(\'' . wp_get_attachment_url($attachment_id, 'medium') . '\')"></div>';
        }
        $html .= '</div>
        </div>
        <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">';
        $html .= '<a class="swiper-slide" href="' . get_the_post_thumbnail_url($post->ID, 'full') . '" data-rel="lightbox"><div class="swiper-slide" style="background-image: url(\'' . get_the_post_thumbnail_url($post->ID, 'medium') . '\')"></div></a>';
        foreach ($attachment_ids as $attachment_id) {
            $html .= '<a class="swiper-slide" href="' . wp_get_attachment_url($attachment_id, 'full') . '" data-rel="lightbox"><div class="swiper-slide"  style="background-image: url(\'' . wp_get_attachment_url($attachment_id, 'medium') . '\')"></div></a>';
        }
        $html .= '</div>
        </div>
    </div>';
    } else {
        $html = '<a class="img-holder" href="' . get_the_post_thumbnail_url( $post->ID, 'full' ) . '" data-rel="lightbox"><div class="img-holder" style="width: 100%;"><img src="' . get_the_post_thumbnail_url( $post->ID, 'full' ) . '" /></div></a>';
//        $html = '<a class="img-holder" href="' . get_the_post_thumbnail_url( $post->ID, 'full' ) . '" data-rel="lightbox"><div class="img-holder" style="background-image: url(\'' . get_the_post_thumbnail_url( $post->ID, 'full' ) . '\')"><img src="' . get_the_post_thumbnail_url( $post->ID, 'full' ) . '" /></div></a>';
    }
} else {
    $html = '<div class="img-holder"><img src="' . esc_url( wc_placeholder_img_src() ) . '" /></div>';
}

echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );
?>

