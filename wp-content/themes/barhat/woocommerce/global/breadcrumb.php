<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
if ( ! empty( $breadcrumb ) ) {
    ?>
    <div class="breadcrumbs-wrapper">
        <?php
        foreach ( $breadcrumb as $key => $crumb ) {
            if($key === 0) continue;

            if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
                echo '<a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';
            } else {
                echo '<span>' . esc_html( $crumb[0] ) . '</span>';
            }
            if ( sizeof( $breadcrumb ) !== $key + 1 ) {
                echo '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
            }
        }
        ?>
    </div>
    <?php
}
