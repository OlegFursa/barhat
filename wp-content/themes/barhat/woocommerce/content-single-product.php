<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<?php
if ( post_password_required() ) {
    echo get_the_password_form();
    return;
}
?>

<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="summary entry-summary">
        <?php wc_get_template_part( 'single-product/add-to-cart/simple', '' ); ?>
    </div><!-- .summary -->
</div><!-- #product-<?php the_ID(); ?> -->