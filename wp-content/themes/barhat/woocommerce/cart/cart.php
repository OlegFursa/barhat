<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

do_action( 'woocommerce_before_cart' );

if ( isset( $_POST['checkout_send'] ) ) {
    wc_print_notices();
}
?>

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
    <section>
        <?php
        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
            $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

            if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                ?>
                <div class="cart-item p-b5">
                    <div class="img-holder with-border" style="background-image: url(<?php echo get_the_post_thumbnail_url( $_product->id, 'full' ); ?>)"></div>
                    <div class="cart-item-bio">
                        <h2 class="f-secodary">
                            <?php
                            if ( ! $product_permalink ) {
                                echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
                            } else {
                                echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
                            }

                            // Meta data
                            echo WC()->cart->get_item_data( $cart_item );

                            // Backorder notification
                            if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
                            }
                            ?>
                        </h2>
                        <div >
                            <?php cart_attribute_values_and_price($cart_item, $cart_item);
                            if (!$cart_item['data']->get_attributes()['pa_running_meter']) {
                                echo '
                                    <div class="d-flex p-r3">
                                        <span class=" p-r1">Кол-во:</span>
                                        <span class="f-secodary">' . $cart_item['quantity'] . '</span>
                                    </div>
                                ';
                            }
                            echo '
                                <div class="d-flex">
                                    <span class=" p-r1">' . __('Price', 'barhat') . ':</span>
                                    <span class="f-secodary">' . $cart_item['data']->get_price() . ' <span class="f-primary">' . get_woocommerce_currency_symbol() . $running_meter . '</span></span>
                                </div>
                            ';

                            ?>
                        </div>
                    </div>
                    <div class="total-block">
                        <span><?php echo __('Total', 'barhat'); ?>:</span>
                        <span class="f-24"><?php echo apply_filters( 'woocommerce_cart_item_subtotal', preg_replace( '/[^.\d]/', '', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ) ), $cart_item, $cart_item_key ); ?><span class="f-16"><?php echo get_woocommerce_currency_symbol(); ?></span></span>
                    </div>
                    <div>
                        <?php
                            echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                            '<a href="%s" aria-label="%s" data-product_id="%s" data-product_sku="%s" class="del-btn"></a>',
                            esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                            __( 'Remove this item', 'woocommerce' ),
                            esc_attr( $product_id ),
                            esc_attr( $_product->get_sku() )
                            ), $cart_item_key );
                        ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </section>

</form>
<?php do_action( 'woocommerce_cart_collaterals' ); ?>
