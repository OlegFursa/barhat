<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>
<section>
    <div class="max-w-1000 m-center pad-l3 pad-r3">
        <div class="total">
            <div class="p-r4 f-18 bold"><?php _e( 'Total', 'woocommerce' ); ?>: </div><div class="f-24" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>"><?php echo preg_replace( '/[^.\d]/', '', wc()->cart->get_total() ); ?> <span class="f-16"><?php echo get_woocommerce_currency_symbol(); ?></span></div>
        </div>
        <div class="order-form">
            <h2 class="f-secodary f-18"><?php echo __('Checkout', 'barhat'); ?></h2>
            <form action="" method="post">
                <input type="text" name="order_name" pattern=".{2,}" placeholder="<?php echo __('Enter your name', 'barhat'); ?>..." required="required">
                <input type="tel" name="order_phone" pattern=".{7,}" placeholder="<?php echo __('Enter phone number', 'barhat'); ?>..." required="required">
                <button class="button button-pink" type="submit" name="checkout_send"><?php echo __('Checkout', 'barhat'); ?></button>
            </form>
        </div>
    </div>
</section>