<?php
if ( ! defined( 'ABSPATH' ) ) {
exit; // Exit if accessed directly
}

get_header( 'shop' );
shop_before_prod();
?>
<div>
    <div class="d-flex f-align-top p-b5 flex-start">
        <?php
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
                wc_get_template_part( 'content', 'product' );
            endwhile;
        elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) :
            do_action( 'woocommerce_no_products_found' );
        endif; ?>
    </div>
    <?php pagination(); ?>
    <div class="category-desc">
        <?php
        $cat = get_term(get_current_category_id(),'product_cat');
        echo translate_q($cat->description);
        ?>
    </div>
</div>
<?php
shop_after_prod();
get_footer( 'shop' ); ?>