<?php get_header(); ?>
    <section>
        <?php echo do_shortcode("[huge_it_slider id='5']"); ?>
    </section>
    <section class="advantages-section">
        <div class="max-w-1000 m-center pad-l3 pad-r3 d-flex">
            <div>
                <img src="/dist/images/illustration/painting-icon.png" alt="">
                <span class="upper"><?php echo __('EXCLUSIVE', 'barhat'); ?></span>
            </div>
            <div>
                <img src="/dist/images/illustration/cinema.png" alt="">
                <span class="upper"><?php echo __('HIGH QUALITY', 'barhat'); ?></span>
            </div>
            <div>
                <img src="/dist/images/illustration/paint-palette-icon.png" alt="">
                <span class="upper"><?php echo __('75 FLOWERS', 'barhat'); ?></span>
            </div>
            <div>
                <img src="/dist/images/illustration/dress-icon.png" alt="">
                <span class="upper"><?php echo __('IN STOCK AND ON ORDER', 'barhat'); ?></span>
            </div>
            <div>
                <img src="/dist/images/illustration/icon.png" alt="">
                <span class="upper"><?php echo __('WHOLESALE AND RETAIL', 'barhat'); ?></span>
            </div>
        </div>
    </section>
    <section>
        <div class="text-c">
            <h2 class="text-c upper decor">Французские ткани</h2>
        </div>
        <div class="max-w-600 m-center pad-l3 pad-r3 text-c p-b4">Ткани, достойные королей, произведенные на семейных французских фабриках, хранящих лучшие традиции,
            представленные в Украине эксклюзивно, в наличии и под заказ, на основе натурального шелка, самого высокого
            качества.
        </div>
        <div class="max-w-1000 m-center pad-l3 pad-r3">
            <div class="tabs-container">
                <div class="tabs-wrapper">
                    <?php
                    $args = array(
                        'taxonomy' => 'product_cat',
                        'child_of' => 55
                    );
                    $all_fabrics55 = get_categories($args);
                    $args = array(
                        'taxonomy' => 'product_cat',
                        'child_of' => 56
                    );
                    $all_fabrics56 = get_categories($args);
                    $all_fabrics = array_merge($all_fabrics55, $all_fabrics56);
                    ?>
                    <nav class="category-nav p-b3 swiper-container nav-slider">
                        <div class="swiper-wrapper">
                            <span class="nav-item swiper-slide upper switch active"
                                  data-tab="tab-0"><?php echo __('all', 'barhat'); ?></span>
                            <?php $i = 1;
                            foreach ($all_fabrics as $f) { ?>
                                <span class="nav-item swiper-slide upper switch"
                                      data-tab="tab-<?= $i ?>"><?php echo $f->name; ?></span>
                                <?php $i++;
                            } ?>
                        </div>
                    </nav>
                    <div class="d-flex f-align-top p-b5 tabs-content active" id="tab-0">
                        <?php
                        $args = array(
                            'post_type' => 'product',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'pa_homepage',
                                    'terms' => 'yes',
                                    'field' => 'slug',
                                    'operator' => 'IN'
                                )
                            ),
                            'posts_per_page' => 4

                        );
                        $loop = new WP_Query($args);
                        $ptemp = array();
                        while ($loop->have_posts()) : $loop->the_post();
                            get_template_part('template-parts/product_in_list', '');
                        endwhile;
                        wp_reset_query();
                        ?>
                    </div>
                    <?php
                    $i = 1;
                    foreach ($all_fabrics as $f) { ?>
                        <div class="d-flex f-align-top p-b5 tabs-content" id="tab-<?= $i ?>">
                            <?php
                            $args = array(
                                'post_type' => 'product',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'product_cat',
                                        'field' => 'slug',
                                        'terms' => $f->slug
                                    ),
                                    array(
                                        'taxonomy' => 'pa_homepage',
                                        'terms' => 'yes',
                                        'field' => 'slug',
                                        'operator' => 'IN'
                                    )
                                ),
                                'posts_per_page' => 4

                            );
                            $loop = new WP_Query($args);
                            $ptemp = array();
                            while ($loop->have_posts()) : $loop->the_post();
                                get_template_part('template-parts/product_in_list', '');
                            endwhile;
                            wp_reset_query();
                            ?>
                        </div>
                        <?php $i++;
                    } ?>
                </div>
            </div>

            <div class="text-c">
                <a href="<?php echo get_term_link(77, 'product_cat'); ?>"
                   class="button button-black m-center"><?php echo __('In store', 'barhat'); ?></a>
            </div>
        </div>
    </section>
    <section>
        <div class="text-c">
            <h2 class="text-c upper decor"><?php echo get_term_by('id', 20, 'product_cat')->name; ?></h2>
        </div>
        <div class="max-w-600 m-center pad-l3 pad-r3 text-c p-b4">YEVA - это неповторимый женственный образ,
            созданный только для вас, и только из натуральных материалов лучших европейских производителей.
        </div>
        <div class="max-w-1000 m-center pad-l3 pad-r3">
            <div class="d-flex p-b5">
                <?php
                $args = array(
                    'taxonomy' => 'product_cat',
                    'child_of' => 57
                );
                $all_categories57 = get_categories($args);
                $args = array(
                    'taxonomy' => 'product_cat',
                    'child_of' => 58
                );
                $all_categories58 = get_categories($args);
                $all_categories = array_merge($all_categories57, $all_categories58);
                foreach ($all_categories as $cat) {
                    $category_id = $cat->term_id;
                    $caturl = get_term_link($cat->slug, 'product_cat');
                    $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                    $catimg = wp_get_attachment_url($thumbnail_id);
                    ?>
                    <a href="<?php echo $caturl; ?>" class="cat-preview p-b4">
                        <div class="image" style="background-image: url('<?php echo $catimg; ?>')"></div>
                        <div class="decor-border"></div>
                        <span class="upper title"><?php echo $cat->name; ?></span>
                    </a>
                    <?php

                }
                ?>
            </div>
            <div class="text-c">
                <a href="<?php echo get_term_link(20, 'product_cat'); ?>"
                   class="button button-black m-center"><?php echo __('In store', 'barhat'); ?></a>
            </div>
        </div>
    </section>
    <section>
        <div class="gallery pad-l3 pad-r3">
            <a href="<?php echo get_permalink(35); ?>"
               class="upper gallery-item gallery-item-link"><?php echo get_the_title(35); ?></a>
            <?php
            $gallery = get_post_gallery(35, false);
            $ids = explode(",", $gallery['ids']);

            $i = 0;
            foreach ($ids as $id) {
                if ($i < 5) {
                    $addclass = '';
                    if ($i == 1) {
                        $addclass = ' first';
                    } else if ($i == 2) {
                        $addclass = ' second';
                    }
                    ?>
                    <div class="gallery-item<?php echo $addclass; ?>"><a
                                href="<?php echo wp_get_attachment_url($id); ?>" data-rel="lightbox">
                            <div class="gallery-item"
                                 style="background-image: url('<?php echo wp_get_attachment_url($id); ?>')"></div>
                        </a></div>
                    <?php
                }
                $i++;
            }
            ?>
        </div>
    </section>
<?php
$news_count = 2;
$whith_button = true;
$title = 'Новости';
include(locate_template('template-parts/news_after_content.php', false, false)); ?>

<?php get_footer(); ?>