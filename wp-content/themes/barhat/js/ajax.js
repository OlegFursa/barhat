var offset_gallery=1;
jQuery( document ).on( 'click', '.load_next', function() {
    jQuery.ajax({
        url : ajax_object.ajax_url,
        type : 'post',
        data : {
            action : 'load_next_gallery',
            page : offset_gallery
        },
        success : function( response ) {
            jQuery('.gallery').append(response);
            offset_gallery ++;
        }
    });
});