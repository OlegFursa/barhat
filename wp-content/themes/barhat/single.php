<?php get_header(); ?>

<section class="max-w-1000 m-center pad-l3 pad-r3">
    <?php do_action( 'woocommerce_before_main_content' ); ?>
<!--    <h1>--><?php //the_title(); ?><!--</h1>-->
    <div class="content">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php
        $current_post_id = get_the_ID();
        endwhile; ?>
    </div>
</section>
<?php
$news_count = 2;
$whith_button = false;
$title = __('Other news', 'barhat');
include( locate_template( 'template-parts/news_after_content.php', false, false ) ); ?>
<?php get_footer(); ?>
