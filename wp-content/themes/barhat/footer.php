</div>
<footer>
    <div class="max-w-1200 m-center d-flex pad-r1 pad-l1">
        <a href="/" class="logo"><img src="/dist/images/interface/Barhat_Logo_white.png" alt=""></a>
        <div class="nav-wrapper ">
            <nav class="footer-nav">
                <?php
                wp_nav_menu(
                    array (
                        'menu' => 26,
                        'container' => '', // parent container
                        'container_id' => '', //parent container ID
                        'depth' => 1,
                        'items_wrap' => '%3$s', // removes ul
                        'walker' => new Custom_menu_template // custom walker to replace li with div
                    )
                );
                ?>
            </nav>
        </div>
        <div class="soc-set">
            <a href="https://www.facebook.com/Barhat-805045603004469" class="soc-btn p-r2"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="https://www.instagram.com/l_barhat_l/" class="soc-btn p-r2"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        </div>
    </div>
</footer>

</body>
</html>