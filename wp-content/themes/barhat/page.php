<?php get_header(); ?>

<section class="max-w-1000 m-center pad-l3 pad-r3">
<?php do_action( 'woocommerce_before_main_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
    <section>
<!--            <h1>--><?php //the_title(); ?><!--</h1>-->
            <?php the_content(); ?>
    </section>
<?php endwhile; // End of the loop. ?>
</section>

<?php get_footer(); ?>
