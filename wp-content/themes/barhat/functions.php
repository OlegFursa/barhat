<?php
// Create custom pagination template
function pagination($pages = '', $range = 2)
{
    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        echo "<div class=\"pagination-wrapper\">";
        if($paged > 1) echo "<a href=\"".get_pagenum_link($paged - 1)."\" class=\"control\"><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></a>";
        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<a class='page-link active'>".$i."</a>":"<a href='".get_pagenum_link($i)."' class='page-link' >".$i."</a>";
            }
        }
        if ($paged < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\" class=\"control\"><i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></a>";
        echo "</div>\n";
    }
}

// Set 8 product per page in category
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 8;' ), 20 );

// Fix multilingual bags | run to display text of current language
function translate_q ($echo) {
    if (function_exists('qtranxf_split')) {
        $selectLanguage = qtranxf_split($echo);
        return $selectLanguage[qtranxf_getLanguage()];
    } else {
        return $echo;
    }
}

if (!function_exists('mb_ucfirst')) {
    function mb_lcfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
        $first_letter = mb_strtolower(mb_substr($str, 0, 1, $encoding), $encoding);
        $str_end = "";
        if ($lower_str_end) {
            $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        }
        else {
            $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
        $str = $first_letter . $str_end;
        return $str;
    }
}

function get_current_category_id() {
    $cate = get_queried_object();
    if ($cate->term_id) {
        $current_cat = $cate->term_id;
    } else {
        $term_list = wp_get_post_terms(get_the_ID(), 'product_cat', array('fields' => 'ids'));
        foreach ($term_list as $t) $current_cat = $t;
    }

    return $current_cat;
}

function get_current_category_id_array() {
    $current_cat = array();
    $cate = get_queried_object();
    if ($cate->term_id) {
        $current_cat[] = $cate->term_id;
        $current_cat[] = $cate->parent;
    } else {
        $term_list = wp_get_post_terms(get_the_ID(), 'product_cat', array('fields' => 'ids'));
        $current_cat = $term_list;
    }
    return $current_cat;
}

function get_parent_category_id() {
    $cate = get_queried_object();
    $current_cat = get_current_category_id();
    $home_categories = get_categories(array(
        'parent' => 0,
        'taxonomy' => 'product_cat'
    ));
    $home_cat_ids = array();
    foreach ($home_categories as $hc) {
        $home_cat_ids[] = $hc->cat_ID;
    }
    if ($cate->term_id) {
        $category_ID = (in_array($cate->parent, $home_cat_ids)) ? $cate->parent : $cate->term_id;
        if (get_ancestors($category_ID, 'product_cat')[0]) $category_ID = get_ancestors($category_ID, 'product_cat')[0];
        if (get_ancestors($category_ID, 'product_cat')[0]) $category_ID = get_ancestors($category_ID, 'product_cat')[0];
    } else {
        $term_list = wp_get_post_terms(get_the_ID(), 'product_cat', array('fields' => 'ids'));
        foreach ($term_list as $c) {
            if (get_ancestors($c, 'product_cat')) {
                $parentcats = get_ancestors($c, 'product_cat');
            } else {
                continue;
            }
            if (get_ancestors($parentcats[0], 'product_cat')[0]) $parentcats = get_ancestors($parentcats[0], 'product_cat');
            if (get_ancestors($parentcats[0], 'product_cat')[0]) $parentcats = get_ancestors($parentcats[0], 'product_cat');
        }
        $category_ID = (!in_array($current_cat, $home_cat_ids)) ? $parentcats[0] : $current_cat;
    }

    return $category_ID;
}

// Union headers and footers for shop pages
function shop_before_prod(){
    $shop_slag = get_post_field( 'post_name', 4 );
    $category_ID = get_parent_category_id();
    $current_cat = get_current_category_id();
    $current_cat_array = get_current_category_id_array();

/*    $args = array(
        'taxonomy'     => 'product_cat',
        'orderby'     => 'term_order',
        'child_of'     => $category_ID
    );*/
    $args = array(
        'child_of' => $category_ID,
        'taxonomy'     => 'product_cat'
    );
    $all_categories = get_categories( $args );
    $prod_term=get_term($category_ID,'product_cat');
    $description=$prod_term->description;
    ?>
    <section>
    <div class="max-w-1000 m-center pad-l3 pad-r3">
    <?php
    do_action( 'woocommerce_before_main_content' );
    ?>
<!--    <h1>--><?php //echo __('Store', 'barhat'); ?><!--</h1>-->
    <section class="tabs-container">
    <?php wc_print_notices(); ?>
    <div class="tabs-wrapper">
    <nav class="category-nav market-nav p-b3">
        <?php $home_categories = get_categories(array(
                'parent' => 0,
                'taxonomy' => 'product_cat'
        ));
        $home_cat_ids = array();
        foreach ($home_categories as $hc) {
            $home_cat_ids[] = $hc->cat_ID;
            ?>
            <a class="nav-item upper switch<?php if ($category_ID == $hc->cat_ID && '/' . $shop_slag . '/' != $_SERVER['REQUEST_URI']) echo ' active'; ?>" href="<?php echo get_term_link( $hc->cat_ID, 'product_cat' ); ?>"><?php echo get_term_by( 'id', $hc->cat_ID, 'product_cat' )->name; ?></a>
        <?php } ?>
    </nav>
    <div id="tab-1" class="tabs-content active market-grid">
    <aside>
        <ul class="sidebar-nav">
            <?php
            $cat_name = '';
            $cid = $current_cat;
            if ('/' . $shop_slag . '/' == $_SERVER['REQUEST_URI'] || !in_array($cid, $home_cat_ids)) $cid = $category_ID;

            if( $term = get_term_by( 'id', $cid, 'product_cat' ) ){
                $cat_name = $term->name;
            }
            ?>
            <li><a href="<?php echo get_term_link( $category_ID, 'product_cat' ); ?>" <?php if (in_array($current_cat, $home_cat_ids) && '/' . $shop_slag . '/' != $_SERVER['REQUEST_URI'] && in_array(0, $current_cat_array)) echo ' class="active"'; ?>><?php _e('All ' . $cid, 'barhat'); echo ' ' . mb_lcfirst($cat_name); ?></a></li>
            <?php
            foreach ($all_categories as $cat) {
                $caturl = get_term_link($cat->slug, 'product_cat');
                $class = '';
                $home_cat_ids[] = 0;
                if (!in_array($cat->category_parent, $home_cat_ids)) {
                    $class = ' child';
                }
                ?>
                <li class="<?php echo $class; ?>"><a href="<?php echo $caturl; ?>"<?php if (in_array($cat->term_id, $current_cat_array) && '/' . $shop_slag . '/' != $_SERVER['REQUEST_URI']) echo ' class="active"'; ?>><?php echo $cat->name; ?></a></li>
                <?php
            }
            ?>
        </ul>
    </aside>
    <?php
}
function shop_after_prod() {
    ?>
                </div>
            </div>
         </section>
        </div>
    </section>
    <?php
}

// Ajax post loader
// Accessing ajax
function my_enqueue() {

    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/js/ajax.js', array('jquery') );

    wp_localize_script( 'ajax-script', 'ajax_object',
        array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue' );

// Ajax response data
add_action( 'wp_ajax_nopriv_load_next_gallery', 'load_next_gallery' );
add_action( 'wp_ajax_load_next_gallery', 'load_next_gallery' );
function load_next_gallery() {
    $page = ($_POST['page']) ? $_POST['page'] : 0;
    $galleries = get_post_galleries(35, false);
    if ($galleries[$page]) {
        $ids = explode(",", $galleries[$page]['ids']);
        foreach ($ids as $id) { ?>
            <div class="gallery-item"><a href="<?php echo wp_get_attachment_url($id); ?>" data-rel="lightbox"><div class="gallery-item" style="background-image: url('<?php echo wp_get_attachment_url($id); ?>')"></div></a></div>
        <?php }
    }

    if ($page != 0) die();
}

// Union news template through all pages
function news_template($ptitle = '', $pid = '') { ?>
    <a href="<?php echo get_permalink($pid); ?>" class="news-item" style="background-image: url('<?php echo get_the_post_thumbnail_url($pid); ?>')">
        <div>
            <span><?php echo $ptitle; ?></span>
        </div>
    </a>
<?php }

// Set Search URL format /search/query
function fb_change_search_url_rewrite() {
    if ( is_search() && ! empty( $_GET['s'] ) ) {
        wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
        exit();
    }
}
add_action( 'template_redirect', 'fb_change_search_url_rewrite' );

function get_product_category_by_id( $category_id ) {
    $term = get_term_by( 'id', $category_id, 'product_cat', 'ARRAY_A' );
    return $term['name'];
}

// Add size to product parameters in cart
add_filter( 'woocommerce_add_cart_item_data', 'add_cart_item_custom_data_vase', 10, 2 );
function add_cart_item_custom_data_vase( $cart_item_meta, $product_id ) {
    global $woocommerce;
    $cart_item_meta['attribute_pa_size'] = $_POST['attribute_pa_size'];
    return $cart_item_meta;
}

// Show size or length attribute in cart
function cart_attribute_values_and_price( $cart_item, $cart_item_key ) {

    $item_data = $cart_item_key['data'];
    $attributes = $item_data->get_attributes();

    if ( ! $attributes ) {
        return $cart_item;
    }

    $out = '';

    foreach ( $attributes as $attribute ) {

        // skip variations
        if ( $attribute->get_variation() ) {
            continue;
        }

        $name = $attribute->get_name();
        if ( $attribute->is_taxonomy() ) {

            $product_id = $item_data->get_id();
            $terms = wp_get_post_terms( $product_id, $name, 'all' );

            $tax_terms = array();
            foreach ( $terms as $term ) {
                $single_term = esc_html( $term->name );
                array_push( $tax_terms, $single_term );
            }
            $attr_vals = implode(', ', $tax_terms);
            // get the taxonomy
            $tax = $terms[0]->taxonomy;

            // get the tax object
            $tax_object = get_taxonomy($tax);

            // get tax label
            if ( isset ( $tax_object->labels->singular_name ) ) {
                $tax_label = $tax_object->labels->singular_name;
            } elseif ( isset( $tax_object->label ) ) {
                $tax_label = $tax_object->label;
                // Trim label prefix since WC 3.0
                $label_prefix = 'Product ';
                if ( 0 === strpos( $tax_label,  $label_prefix ) ) {
                    $tax_label = substr( $tax_label, strlen( $label_prefix ) );
                }
            }

            $running_meter = '';
            if ($name == 'pa_size') {
                /* <span class="f-secodary">' . $attr_vals . '</span> */
                $out .= '
                <div class="d-flex p-r3">
                    <span class=" p-r1">' . $tax_label . ':</span>
                    <span class="f-secodary">' . urldecode($cart_item['attribute_pa_size']) . '</span>
                </div>
                ';

                break;
            } else if ($name == 'pa_running_meter') {
                $out .= '
                <div class="d-flex p-r3">
                    <span class=" p-r1">' . __('Length', 'barhat') . ':</span>
                    <span class="f-secodary">' . $cart_item['quantity'] . 'м</span>
                </div>
                ';
                $running_meter = '/метр';
                break;
            }

            // end for taxonomies
        }
    }
    echo $out;
}

// Remove woocommerce CSS to fix override theme CSS
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

// Add theme support for widgets.
add_theme_support( 'customize-selective-refresh-widgets' );

// Add widget column for language.
if ( function_exists('register_sidebar') )
    register_sidebar(array('name'=>'sidebar',
        'id' => 'sidebar',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<div class="title">',
        'after_title' => '</div>',
    ));

// Begin redefine widget "language switcher" For custom template and functional
function qtranxfD2_generateLanguageSelectCode($class = '') {
    global $q_config;
    $flag_location=qtranxf_flag_location();
    $format = '%c';
    foreach(qtranxf_getSortedLanguages() as $language) {
        $alt = $q_config['language_name'][$language].' ('.$language.')';
        $s = $flag_location.$q_config['flag'][$language];
        $n = $q_config['language_name'][$language];
        $content = $format;
        $content = str_replace('%f', '<img src="'.$s.'" alt="'.$alt.'" />', $content);
        $content = str_replace('%s', $s, $content);
        $content = str_replace('%n', $n, $content);
        if(strpos($content,'%a')!==FALSE){
            $a = qtranxf_getLanguageName($language);
            $content = str_replace('%a', $a==$n ? '' : $a, $content);
        }
        $content = str_replace('%c', $language, $content);
        if($language != $q_config['language'])
            echo '<a class="lang' . $class . ' upper" href="' . qtranxf_convertURL($url, $language, false, true) . '" title="' . $alt . '">' . $content . '</a>';
    }
}

// Set search engine in products only
add_action( 'pre_get_posts', 'wpse223576_search_woocommerce_only' );
function wpse223576_search_woocommerce_only( $query ) {
    if( ! is_admin() && is_search() && $query->is_main_query() ) {
        $query->set( 'post_type', 'product' );
    }
}

// Set custom header and footer menu template
class Custom_menu_template extends Walker_Nav_Menu
{
    function start_el(&$output, $item, $depth, $args)
    {
        $classes = empty($item->classes) ? array () : (array) $item->classes;
        $class_names = join(' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        !empty ( $class_names ) and $class_names = ' class="'. esc_attr( $class_names ) . ' nav-item upper"';
        $attributes  = '';
        !empty( $item->attr_title ) and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
        !empty( $item->target ) and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
        !empty( $item->xfn ) and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
        !empty( $item->url ) and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';
        $title = apply_filters( 'the_title', $item->title, $item->ID );
        $output .= "<a $attributes id='menu-item-$item->ID' $class_names>";
        $item_output = $args->before
            . $args->link_before
            . $title
            . '</a>'
            . $args->link_after
            . $args->after;
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

// Fix active menu in child pages of woocommerce
add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );
function add_current_nav_class($classes, $item) {
    // Getting the URL of the menu item
    $menu_slug = strtolower(trim($item->url));

    // If the menu item URL contains the current post types slug add the current-menu-item class
    if (is_product_category() && preg_match('/shop/', $menu_slug)) {
        $classes[] = 'current-menu-item';
    }
    return $classes;
}

// Determine active menu item
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class ($classes, $item) {
    $classes_to_check = implode($classes);
    if (preg_match('/current/', $classes_to_check)){
        $classes[] = 'active ';
    }
    return $classes;
}

// Show all product attributes on the product page EXCLUDE running_meters
function isa_woocommerce_all_pa(){

    global $product;
    $attributes = $product->get_attributes();
    if ( ! $attributes ) {
        return;
    }

    $out = '';

    foreach ( $attributes as $attribute ) {

        // skip variations
        if ( $attribute->get_variation() ) {
            continue;
        }
        $name = $attribute->get_name();
        if ( $attribute->is_taxonomy() ) {

            $terms = wp_get_post_terms( $product->get_id(), $name, 'all' );
            // get the taxonomy
            $tax = $terms[0]->taxonomy;
            // get the tax object
            $tax_object = get_taxonomy($tax);

            if (!in_array($tax_object->name, array('pa_manufacturer', 'pa_running_meter')) && (count($terms) == 1 && $tax_object->name == 'pa_size')) {
                // get tax label
                if (isset ($tax_object->labels->singular_name)) {
                    $tax_label = $tax_object->labels->singular_name;
                } elseif (isset($tax_object->label)) {
                    $tax_label = $tax_object->label;
                    // Trim label prefix since WC 3.0
                    if (0 === strpos($tax_label, 'Product ')) {
                        $tax_label = substr($tax_label, 8);
                    }
                }
                $out .= '<div class="d-flex">';
                $out .= '<span>' . $tax_label . ':</span>';
                $tax_terms = array();
                foreach ($terms as $term) {
                    $single_term = esc_html($term->name);
                    array_push($tax_terms, $single_term);
                }
                $out .= '<span class="f-secodary">' . implode(', ', $tax_terms) . '</span>';
                $out .= '</div>';
            }

        } else {
            $out .= '<div class="d-flex">';
            $out .= '<span>' . $name . ':</span>';
            $out .= '<span class="f-secodary">' . esc_html( implode( ', ', $attribute->get_options() ) ) . '</span>';
            $out .= '</div>';
        }


    }
    echo $out;
}

// Set mail type html
function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );

// Custom checkout functionality
add_action( 'init', 'woocommerce_checkout_send' );
function woocommerce_checkout_send() {
    global $wpdb;
    global $woocommerce;

    if ( isset( $_POST['checkout_send'] ) ) {
        $order_name = htmlspecialchars(trim($_POST['order_name']));
        $order_phone = htmlspecialchars(trim($_POST['order_phone']));

        $order_number = (int)$wpdb->get_var( "SELECT `id` FROM `" . $wpdb->prefix . "woocommerce_orders` ORDER BY `id` DESC LIMIT 1" ) + 1;

        $wpdb->insert( $wpdb->prefix . 'woocommerce_orders', array(
            'order_number' => $order_number,
            'order_name' => $order_name,
            'order_phone' => $order_phone
        ));

        $to = get_option('admin_email');
        $subject = 'BARHAT Заказ №' . $order_number;
        $message = 'Имя: ' . $order_name . '<br />';
        $message .= 'Телефон: ' . $order_phone . '<br />';
        $message .= 'Товары:<br />';

        $items = $woocommerce->cart->get_cart();

        foreach($items as $item => $values) {
            $_product = $values['data']->post;
            $message .= "<a href='" . get_permalink($_product->ID) . "'><b>".$_product->post_title.'</b></a> <br />Количество: '.$values['quantity'] . '<br />';
            $price = get_woocommerce_currency_symbol() . get_post_meta($values['product_id'] , '_price', true);
            $size = $values['attribute_pa_size'];
            if ($size) {
                $message .= "Размер: ".$size."<br />";
            }
            $message .= "Цена: ".$price."<br /><br />";
        }
        $message .= "Итого: " . $woocommerce->cart->get_cart_total() . "<br /><br />";

        wp_mail( $to, $subject, $message );
        $woocommerce->cart->empty_cart();
        wc_add_notice( '<h2 style="text-align: center;">Спасибо за заказ!<br>
Мы непременно свяжемся с вами в ближайшее время, чтобы обсудить форму и условия
доставки и платежа товара из наличия. А в случае заказной позиции – сроки.
Не забудьте ознакомиться с условиями возврата и обмена наших товаров.</h2>');
    }
}

// Overwrite breadcrumbs functionality to show the deepest category level
include('includes/class-custom-breadcrumb.php');

function woocommerce_breadcrumb( $args = array() ) {
    $args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
        'delimiter'   => '&nbsp;&#47;&nbsp;',
        'wrap_before' => '<nav class="woocommerce-breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>',
        'wrap_after'  => '</nav>',
        'before'      => '',
        'after'       => '',
        'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' )
    ) ) );

    $breadcrumbs = new Custom_Breadcrumb(); //use our customised class

    if ( $args['home'] ) {
        $breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
    }

    $args['breadcrumb'] = $breadcrumbs->generate();

    wc_get_template( 'global/breadcrumb.php', $args );
}