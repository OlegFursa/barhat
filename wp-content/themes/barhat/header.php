<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Barhat</title>

	<meta property="og:title" content="Barhat"/>
	<meta property="og:description" content="Ткани, достойные королей, произведенные на семейных французских фабриках, хранящих лучшие традиции, представленные в Украине эксклюзивно, в наличии и под заказ, на основе натурального шелка, самого высокого качества."/>
	<meta property="og:image" content="http://barhat.kiev.ua/dist/images/interface/Barhat_Logo.png"/>
	<meta property="og:url" content="http://barhat.kiev.ua"/>
	<meta property="og:type" content="website"/>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=0, width=device-width">

    <?php wp_head(); ?>



    <link rel="stylesheet" href="/dist/styles/vendor.min.css">
    <link rel="stylesheet" href="/dist/styles/stylesheet.min.css">
    <link rel="stylesheet" href="<? print get_template_directory_uri(); ?>/style.css">

    <link rel="icon" href="/favicon.ico">

    <script src="/dist/scripts/scripts.js"></script>
</head>
<body>
<div class="main-shell">
    <header class="header">
        <div class="max-w-1200 m-center d-flex pad-r3 pad-l3">
            <a href="/" class="logo"><img src="/dist/images/interface/Barhat_Logo.png" alt=""></a>
            <div class="nav-wrapper">
                <nav class="main-nav">
                    <?php
                    wp_nav_menu(
                        array (
                            'menu' => 26,
                            'container' => '', // parent container
                            'container_id' => '', //parent container ID
                            'depth' => 1,
                            'items_wrap' => '%3$s', // removes ul
                            'walker' => new Custom_menu_template // custom walker to replace li with div
                        )
                    );
                    ?>
                    <?php // qtranxfD2_generateLanguageSelectCode(' mob-lang'); ?>
					<a href="tel:+380965109390" class="bold phone">+38 096 510 93 90</a>
                </nav>

                <a href="tel:+380965109390" class="p-l4 bold phone">+38 096 510 93 90</a>

                <?php // qtranxfD2_generateLanguageSelectCode(); ?>
            </div>
            <div class="user-set">
                <div class="mob-btn">
                    <span></span>
                </div>
                <div class="search-wrapper p-r2">
                    <form id="search-form" method="get" action="/" class="search-wrapper p-r2">
                        <img src="/dist/images/interface/magnify-glass.svg" alt="" id="search-btn">
                        <i class="fa fa-times close-search" aria-hidden="true" ></i>
                        <input name="s" value="<?php echo get_search_query(); ?>" class="search-input" type="search">
                    </form>
                </div>
                <a href="/cart/" class="rel">
                    <img src="/dist/images/interface/bag.svg" alt="">
                    <?php
                    $quantity = 0;
                    foreach (WC()->cart->get_cart_item_quantities() as $q) $quantity += $q;
                    if ($quantity != 0) {
                    ?>
                        <span class="quantity"><?php echo $quantity; ?></span>
                    <?php } ?>
                </a>
            </div>
        </div>
    </header>