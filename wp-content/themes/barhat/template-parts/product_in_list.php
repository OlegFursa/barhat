<a href="<?php echo get_permalink(); ?>" class="product-item">
    <div class="img-holder" style="background-image: url('<?php echo get_the_post_thumbnail_url(null, 'full'); ?>')"></div>
    <div class="product-info">
        <div class="pad-r4">
            <span class="upper f-extra f-15"><?php the_title(); ?></span>
            <span class="d-block"><?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?></span>
        </div>
    </div>
</a>