<section class="max-w-1000 m-center pad-l3 pad-r3 p-b0">
    <?php do_action( 'woocommerce_before_main_content' ); ?>
<!--    <h1>--><?php //echo get_the_title(63); ?><!--</h1>-->
</section>


<section>
    <div class="max-w-1000 m-center pad-l3 pad-r3 d-flex">
        <div class="d-flex flex-start p-b4 search-input-res-wrapper">
            <?php get_search_form(); ?>
            <span><?php echo __('Found', 'barhat'); ?> <?php echo $wp_query->found_posts; ?> <?php echo __('products', 'barhat'); ?></span>
        </div>
        <div class="d-flex f-align-top p-b5 flex-start negative-margin">
            <?php
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/product_in_list', '' );

                endwhile; // End of the loop.
            endif;
            ?>
        </div>
    </div>
    <div class="max-w-1000 m-center pad-l3 pad-r3 d-flex">
    <?php pagination(); ?>
    </div>
</section>
