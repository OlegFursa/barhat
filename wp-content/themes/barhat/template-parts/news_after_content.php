<section>
    <div class="text-c">
        <h2 class="text-c upper decor">Блог</h2>
    </div>
    <div class="max-w-1000 m-center pad-l3 pad-r3">
        <div class="d-flex p-b5">
            <?php
            $cur_post_id = ($current_post_id) ? $current_post_id : '';
            $args = array(
                'numberposts' => ($news_count) ? $news_count : 2,
                'category' => 19,
                'exclude' => array($cur_post_id)
            );
            $latest_posts = get_posts( $args );
            foreach ($latest_posts as $p) {
                $pid = $p->ID;
                $ptitle = $p->post_title;
                news_template($ptitle, $pid);
            }
            ?>
        </div>

        <?php if ($whith_button) { ?>
        <div class="text-c">
            <a href="<?php echo get_category_link(19); ?>" class="button button-black m-center">Все статьи</a>
        </div>
        <?php } ?>
    </div>
</section>