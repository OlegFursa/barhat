<?php
// About page
get_header(); ?>
<div class="max-w-1000 m-center pad-l3 pad-r3">
    <?php do_action( 'woocommerce_before_main_content' ); ?>
<!--    <h1>--><?php //the_title(); ?><!--</h1>-->

</div>
<section class="advantages-section">
    <div class="max-w-1000 m-center pad-l3 pad-r3 d-flex">
        <div>
            <img src="/dist/images/illustration/painting-icon.png" alt="">
            <span class="upper"><?php echo _e('EXCLUSIVE', 'barhat'); ?></span>
        </div>
        <div>
            <img src="/dist/images/illustration/cinema.png" alt="">
            <span class="upper"><?php echo _e('HIGH QUALITY', 'barhat'); ?></span>
        </div>
        <div>
            <img src="/dist/images/illustration/paint-palette-icon.png" alt="">
            <span class="upper"><?php echo _e('75 FLOWERS', 'barhat'); ?></span>
        </div>
        <div>
            <img src="/dist/images/illustration/dress-icon.png" alt="">
            <span class="upper"><?php echo _e('IN STOCK AND ON ORDER', 'barhat'); ?></span>
        </div>
        <div>
            <img src="/dist/images/illustration/icon.png" alt="">
            <span class="upper"><?php echo _e('WHOLESALE AND RETAIL', 'barhat'); ?></span>
        </div>
    </div>
</section>
<?php while ( have_posts() ) : the_post(); ?>
    <section>
        <div class="max-w-1000 m-center pad-l3 pad-r3 d-flex<?php /* ?> about-wrapper<?php */ ?>">
        <?php /* ?><aside>
                <div class="img-holder" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"></div>
            </aside>
        <?php */ ?>
            <div class="content">
                <?php the_content(); ?>
            </div>
        </div>
    </section>
<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
