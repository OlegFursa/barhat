<?php
// Gallery page
get_header(); ?>

<section class="p-b0">
    <div class="max-w-1000 m-center pad-l3 pad-r3 d-flex">
        <?php do_action( 'woocommerce_before_main_content' ); ?>
    </div>
</section>
<?php
while ( have_posts() ) : the_post();

?>
    <section>
        <div class="max-w-1000 m-center pad-l3 pad-r3">
<!--            <h1>--><?php //the_title(); ?><!--</h1>-->
        </div>
        <div class="gallery p-b6">
            <?php load_next_gallery(); ?>
        </div>
        <div class="text-c "><a class="upper primary-link load_next"><?php echo __('show more', 'barhat'); ?></a></div>
    </section>
<?php endwhile; // End of the loop.

get_footer(); ?>
