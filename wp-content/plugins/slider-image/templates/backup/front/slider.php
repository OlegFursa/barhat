<?php
/**
 * @var int $slider_id Slider ID.
 * @var int $show_loading_icon 1 for show, otherwise 0.
 * @var string $loading_icon_type
 * @var Hugeit_Slider_Slide[] $slides
 * @var Hugeit_Slider_Slider $slider
 */
?>

    <gallery-slider class="reset-inherit-styles">
        <div class="swiper-container">
            <div class="swiper-wrapper">
		        <?php foreach ( $slides as $key => $slide ) { ?>
                    <div class="swiper-slide">
                        <img src="<?php echo wp_get_attachment_url($slides[$key]->get_attachment_id()); ?>" alt="<?php echo $slides[$key]->get_title(); ?>" title="<?php echo $slides[$key]->get_description(); ?>" class="block p-b5">
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="swiper-button-prev"><icon class="arr-left"></icon></div>
        <div class="swiper-button-next"><icon class="arr-right"></icon></div>

        <div class="swiper-pagination"></div>
    </gallery-slider>
