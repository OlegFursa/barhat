<?php
/**
 * @var int $slider_id Slider ID.
 * @var int $show_loading_icon 1 for show, otherwise 0.
 * @var string $loading_icon_type
 * @var Hugeit_Slider_Slide[] $slides
 * @var Hugeit_Slider_Slider $slider
 */
?>

<div class="swiper-container main-slider" data-main-slider>
    <div class="swiper-wrapper">
		        <?php foreach ( $slides as $key => $slide ) { ?>
                    <div class="swiper-slide" style="background-image: url('<?php echo wp_get_attachment_url($slides[$key]->get_attachment_id()); ?>')"></div>
                <?php } ?>
    </div>
    <div class="swiper-pagination"></div>
</div>