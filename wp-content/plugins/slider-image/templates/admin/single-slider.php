<?php
/**
 * @var Hugeit_Slider_Slider $slider
 * @var array $all_sliders_id_name_pair
 * @var string $save_slider_nonce
 */
?>

<div class="wrap">
	<?php echo Hugeit_Slider_Template_Loader::render(HUGEIT_SLIDER_ADMIN_TEMPLATES_PATH . DIRECTORY_SEPARATOR . 'free-banner.php'); ?>
    <form action="admin.php?page=hugeit_slider&id=<?php echo $slider->get_id(); ?>" method="post" name="adminForm" id="adminForm">
		<div id="poststuff" >
			<div id="slider-header">
				<ul id="sliders-list">
					<?php
					foreach ($all_sliders_id_name_pair as $pair) :
						if ($pair->id != $slider->get_id()) :
							$edit_page_safe_link = wp_nonce_url('admin.php?page=hugeit_slider&task=edit&id=' . $pair->id, 'edit_slider_' . $pair->id, 'hugeit_slider_edit_slider_nonce');
							?>
							<li data-slider-id="<?php echo $pair->id; ?>">
								<a href="#" onclick="window.location.href='<?php echo $edit_page_safe_link; ?>'" ><?php echo $pair->name; ?></a>
							</li>
						<?php else : ?>
							<li data-slider-id="<?php echo $pair->id; ?>" class="active">
								<input class="text_area" type="text" name="name" id="name" maxlength="250" value="<?php echo $pair->name; ?>" />
							</li>
						<?php endif;
					endforeach;
					?>
					<li class="add-new">
						<a onclick="window.location.href='<?php echo $add_slider_safe_link; ?>'">+</a>
					</li>
				</ul>
			</div>
			<div id="post-body" class="metabox-holder columns-2">
                <div class="save-result">
                    <p class="message"></p>
                </div>
                <div id="zoomed-image-section">
                    <img src="" alt="zoomed image" />
                </div>
				<div id="post-body-content">
					<?php add_thickbox(); ?>
					<div id="hugeit-slider-add-video-popup" style="display: none;">
						<?php echo Hugeit_Slider_Html_Loader::get_video_slide_popup_html(); ?>
					</div>
					<div id="hugeit-slider-add-post-popup" style="display: none;">
						<?php echo Hugeit_Slider_Html_Loader::get_post_slide_popup_html(); ?>
					</div>

					<div id="post-body">
						<div id="post-body-heading">
							<h3><?php _e('Slides', 'hugeit-slider'); ?></h3>
							<span class="wp-media-buttons-icon"></span>

							<div id="hugeit_add_slide_buttons_wrapper">
                                <button type="button" id="hugeit_slider_add_image_slide_button" class="button button-primary button-large"><?php _e('Add Image Slide', 'hugeit-slider'); ?></button>
                            </div>

						</div>
						<ul id="slides-list" class="images-list-block">
							<?php
							$has_background = true;

							$slides = $slider->get_slides();
							/**
							 * @var Hugeit_Slider_Slide_Image $slide
							 */
							foreach ( $slides as $order => $slide ) :
								switch ($slide->get_type()) :
									case 'image' : ?>
										<li data-type="<?php echo 'image'; ?>" class="slider-cell" data-slide-id="<?php echo $slide->get_id(); ?>" data-order="<?php echo $order; ?>">
											<?php echo Hugeit_Slider_Html_Loader::get_slide_html($slide); ?>
										</li>
										<?php
										break;
								endswitch;
							endforeach; ?>
						</ul>
					</div>

				</div>

				<!-- SIDEBAR -->
				<div id="postbox-container-1" class="postbox-container">
                    <div id="side-sortables1" class="meta-box-sortables ui-sortable">
                        <div id="slider-options" class="postbox">
                            <div id="major-publishing-actions">
                                <div id="publishing-action">
                                    <span class="spinner" id="hugeit_slider_save_slider_spinner"></span>
                                    <input type="submit" value="<?php _e('Save Slider', 'hugeit-slider'); ?>" id="save-buttom" class="button button-primary button-large" data-nonce="<?php echo $save_slider_nonce; ?>">
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div id="slider-shortcode-box" class="postbox shortcode ms-toggle">
						<h3 class="hndle"><span><?php _e('Shortcodes', 'hugeit-slider'); ?></span></h3>
						<div class="inside">
							<ul>
								<li rel="tab-1" class="selected">
									<h4><?php _e('Shortcode for posts/pages/plugins', 'hugeit-slider'); ?></h4>
									<p><?php _e('Copy &amp; paste the shortcode directly into any WordPress post or page', 'hugeit-slider'); ?>.</p>
									<textarea class="full" readonly="readonly">[huge_it_slider id="<?php echo $slider->get_id(); ?>"]</textarea>
								</li>
								<li rel="tab-2">
									<h4><?php _e('Shortcode for templates/themes', 'hugeit-slider'); ?></h4>
									<p><?php _e('Copy &amp; paste this code into a template file to include the slideshow within your theme', 'hugeit-slider'); ?>.</p>
									<textarea class="full" readonly="readonly">&lt;?php echo do_shortcode("[huge_it_slider id='<?php echo $slider->get_id(); ?>']"); ?&gt;</textarea>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>