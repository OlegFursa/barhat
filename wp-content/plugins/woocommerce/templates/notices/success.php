<?php
/**
 * Show messages
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/notices/success.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! $messages ) {
	return;
}

?>

<?php foreach ( $messages as $message ) : ?>
	<div class="woocommerce-message 123"><?php echo wp_kses_post( $message ); ?></div>
<!--    <div class="contact-wrapper">-->
<!--        <div class="contact-info success-block">-->
<!--            <div>-->
<!--                <span>Офис-склад</span>-->
<!--                <p>г. Киев, Подол, ул. Спасская, 31 Б офис 101<br> Режим работы: Пн-Пт с 10:00 до 18:00</p>-->
<!--                <span>Магазин</span>-->
<!--                <p>г. Киев, Воздвиженка,<br> ул. Кожемяцкая, 20 Б<br> Режим работы: Пн-Пт с 11:00 до 20:00</p>-->
<!--            </div>-->
<!--            <div>-->
<!--                <span>Контактные номера</span>-->
<!--                <p>Зоряна (Бархат):<br> +38 096 420 35 49; <br> +38 067 785 90 43 info.barhat.kiev@gmail.com <br><br> Ольга-->
<!--                    (одежда и аксессуары "YEVA"):<br> +380505085565, omarinenko@gmail.com <br>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

<?php endforeach; ?>
